from flask import Flask, render_template, redirect, request, flash, jsonify, session, send_file
from werkzeug.utils import secure_filename
import os
import sys
import re
import glob
import numpy
import shutil
import math


app = Flask(__name__)
numpy.set_printoptions(threshold=sys.maxsize)
app.secret_key = "caircocoders-ednalan"
UPLOAD_FOLDER = 'static/uploads'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
  
@app.route('/')
def index():
    return render_template('index.html')

@app.route("/download")
def download(): 
    files = glob.glob(app.config['UPLOAD_FOLDER'] +'/*')
    DecodeFileName = 'static/decoders/decode.' + files[0].split('.')[-1]
    shutil.copyfile(files[0], DecodeFileName)

    folder = app.config['UPLOAD_FOLDER']
    for filename in os.listdir(folder):
        file_path = os.path.join(folder, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            print('Failed to delete %s. Reason: %s' % (file_path, e))

    # Отправляем архим с документами пользователю
    return send_file(DecodeFileName,
            cache_timeout = -1,
            as_attachment = True)

@app.route("/upload",methods=["POST","GET"])
def upload():
    binary_str = ''
    if request.method == 'POST':
        files = request.files.getlist('files[]')
        for file in files:
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            with open(app.config['UPLOAD_FOLDER'] +'/'+ filename, 'rb') as f:
                data = f.read()
            dec_str = ' '.join([str(int(i)) for i in data])
            hexdata = bin(int(data.hex(), 16))[2:]
            bin_str = ' | '.join(re.findall('.{%s}' % 8, hexdata))
            f.close()
        flash('Файл успешно добавлен!')       
    return render_template('index.html', data_dec = dec_str, data_bin = bin_str)


@app.route("/insert",methods=["POST","GET"])
def insert():  
    mas = [0]*15
    def strBinToInt(str):
        return int(str, 2)

    def intToBinStr(number):
        return bin(number)

    #global variable for caching
    actual_fibonacci_numbers = [
        1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765, 10946,
        17711, 28657, 46368, 75025, 121393, 196418, 317811, 514229, 832040, 1346269, 2178309, 3524578,
        5702887, 9227465, 14930352, 24157817, 39088169
    ]
    def encodeFibonacci(number, string_output = False):
        fibonacci_numbers = actual_fibonacci_numbers

        if number < 1:
            return  0

        output_bin = 1
        generate_next = True

        i = 0
        for n in fibonacci_numbers:
            if n > number:
                generate_next = False
                break
            i += 1

        while generate_next:
            fib = sum(fibonacci_numbers[-2:])
            fibonacci_numbers.append(fib)
            if fib > number:
                break
            i += 1

        for fib in reversed(fibonacci_numbers[0:i]):
            output_bin = output_bin << 1
            if number >= fib:
                output_bin += 1
                number -= fib

        if string_output:
            return intToBinStr(output_bin)
        return output_bin

    def decodeFibonacci(binary, string_input = False):
        if string_input:
            binary = strBinToInt(binary)
        fib_list = [1, 1]
        number = 0
        was_one = False
        mask = 1

        while binary > mask:
            if binary & mask > 0:
                if was_one:
                    break
                number += fib_list[-1]
                was_one = True
            else:
                was_one = False
            fib_list.append(sum(fib_list[-2:]))
            mask <<= 1
        return number



    if request.method == 'POST':
        insert = request.form['checkboxvalue']
        Dec_str = insert.split(' ')

        encode_bytes = ''
        for byte in Dec_str:
            if int(byte) == 0:
                encode_bytes += '011' + ' | '
            else:
                byte_enc = str(encodeFibonacci(int(byte), True))[2:][::-1] + ' | '
                encode_bytes += byte_enc
                mas[len(byte_enc)-3] += 1
        H = 0
        mas_en_bits = encode_bytes.split(' | ')
        UniqSymb = list(set(mas_en_bits))

        for s in UniqSymb:
            Pi = mas_en_bits.count(s)/len(mas_en_bits)
            H += Pi*math.log2(Pi)
        H = -1*H
        Sred_len = 0
        for i in range(0,15):
            Sred_len += mas[i] / len(Dec_str) * i 

        Decode_Dec = ''
        decode_bytes = encode_bytes.split(' | ')[:-1]
        for byte in decode_bytes:
            Decode_Dec += str(decodeFibonacci(byte[::-1], True)) + ' '
        folder = 'static/decoders'
        for filename in os.listdir(folder):
            file_path = os.path.join(folder, filename)
            try:
                if os.path.isfile(file_path) or os.path.islink(file_path):
                    os.unlink(file_path)
                elif os.path.isdir(file_path):
                    shutil.rmtree(file_path)
            except Exception as e:
                print('Failed to delete %s. Reason: %s' % (file_path, e))
        
        
        
        N = "N_sred = " + str(Sred_len) + "\nH = " + str(H)  
        return jsonify({'success' : "Файл закодирован!", 'Encode_Bytes': encode_bytes, 'Decode_Bytes' : Decode_Dec, 'N' : N})

if __name__ == "__main__":
    app.run(host = "172.27.14.155", port = 1236)
